# Builder
Kotlin library for the JVM to complement Maven Archetype generators.

## Why
Let's be honest, Maven is very limited. Yes, you can customize the plugins and dependencies that you want to add
to a given project but after that, there is not so much you can do to allow injecting different files or new
code based on certain conditions.

To overcome that, this library takes the output of a regular Archetype and based on configuration it will:

* Add/remove files from the project
* Add or replace code

## Usage
Kotlin syntax
```kotlin
val project = Project("/myprojectpath")
```

Java syntax
```java
Project project = new Project("/myprojectpath");
```


## Example:
```Java
Project(Platform.JAVA).setInitialDir( "/" ).addDependencies( dependeciesList ).excludeFiles( globExp ).inject();
```
Scan for config files pom or build.gradle; if conflict, then error out and specify overloaded method with platform specific
Insert some code at line(x)

