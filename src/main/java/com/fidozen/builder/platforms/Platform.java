package com.fidozen.builder.platforms;

import com.fidozen.builder.platforms.impl.JavaProcessor;
import com.fidozen.builder.platforms.impl.NodeProcessor;

/**
 * List of supported platforms
 */
public enum Platform {

    JAVA(new JavaProcessor()),
    NODEJS(new NodeProcessor());

    private Processor defaultProcessor;

    Platform(Processor p){
        this.defaultProcessor = p;
    }

    /**
     * Returns a particular platform processor: i.e. JavaProcessor, NodeProcessor
     * @return Processor Platform processor
     */
    public Processor getProcessor(){
        return this.defaultProcessor;
    }

}

