package com.fidozen.builder.support;

/**
 * Created by fido on 7/11/16.
 */
public class BuilderException extends Exception {
    public BuilderException(String message){
        super(message);
    }
}
