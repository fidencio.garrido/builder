@XmlSchema(
        namespace = "http://maven.apache.org/POM/4.0.0",
        elementFormDefault = XmlNsForm.QUALIFIED)
package com.fidozen.builder.support.impl.maven;

import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.XmlNsForm;