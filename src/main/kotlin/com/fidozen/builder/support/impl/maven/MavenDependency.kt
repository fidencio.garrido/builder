package com.fidozen.builder.support.impl.maven

import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name="dependency")
class MavenDependency {
    @XmlElement
    val groupId: String? = null
    @XmlElement
    val artifactId: String? = null
    @XmlElement
    val version: String? = null
}