package com.fidozen.builder.support.impl.maven

import javax.xml.bind.annotation.XmlElement
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement(name="dependencies")
class MavenDependencies {
    @XmlElement(name="dependency")
    val dependencies: List<MavenDependency>? = null
}