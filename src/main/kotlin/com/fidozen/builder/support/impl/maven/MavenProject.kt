package com.fidozen.builder.support.impl.maven

import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlElement

@XmlRootElement(name="project")
class MavenProject() {
    @XmlElement
    val modelVersion: String? = null
    @XmlElement
    val groupId: String? = null
    @XmlElement
    val artifactId: String? = null
    @XmlElement
    val version: String? = null
    @XmlElement
    val packaging: String? = null
    @XmlElement
    val url: String? = null
    @XmlElement(name="dependencies")
    val dependencies: MavenDependencies? = null

}