package com.fidozen.builder.support.impl

import com.fidozen.builder.configuration.Dependency
import com.fidozen.builder.support.ConfigurationResource

class MavenConfigurator : ConfigurationResource{
    val project: String?

    constructor(projectDir: String){
        this.project = projectDir
        this.reloadPOM()
    }

    override fun addEntry(dependency: Dependency) {
        throw UnsupportedOperationException()
    }

    override fun addEntries(dependencies: List<Dependency>) {
        throw UnsupportedOperationException()
    }

    /**
     * Reads the POM file and reloads the dependencies
     */
    fun reloadPOM(){

    }

    /**
     *
     */
    private fun readPomAsText(){

    }

    /**
     * Adds a new dependency to the POM file
     * @param groupId Maven's groupId
     * @param artifactId Maven's artifactId
     * @param version Maven's version
     */
    fun addEntry(groupId: String, artifactId: String, version: String){
        /**
         * ToDo:
         * Find the dependencies section and store
         */
        this.reloadPOM()
    }

    /**
     * Takes a common dependency object and convets it to a XML Maven string
     * @param dependency The builder common dependency
     */
    fun dependency2xml(dependency: Dependency) : String{
        val gid = dependency.getPropertyValue("groupId")
        val dep = dependency.name
        val ver = dependency.version
        return "<dependency>\n\t<groupId>$gid</groupId>\n\t<artifactId>$dep</artifactId>\n\t<version>$ver</version>\n</dependency>"
    }
}