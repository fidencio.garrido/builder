package com.fidozen.builder.support.impl.maven

import com.fidozen.builder.configuration.Dependency
import java.io.File
import java.util.ArrayList
import java.io.FileReader
import javax.xml.bind.JAXBContext
import javax.xml.bind.Unmarshaller
import com.fidozen.builder.support.impl.maven.MavenProject

/**
 * POM file reader, the constructor expects only the path to the folder that contains the pom
 */
class POMXMLReader(val pomFolder: String) {
    val POM: String = "pom.xml"
    val pomFile: File?
    var pom: MavenProject? = null

    init{
        this.pomFile = File(pomFolder + File.separator + this.POM)
    }

    fun readPom(){
        val context = JAXBContext.newInstance(MavenProject::class.java)
        val unmarshaller = context.createUnmarshaller()
        this.pom = unmarshaller.unmarshal(FileReader(this.pomFile?.getAbsolutePath())) as MavenProject?
    }

    fun getPom(){
        if (this.pom == null)
            this.readPom()
    }

    fun getDependencies(): List<Dependency>{
        this.getPom()
        var dependencies = ArrayList<Dependency>()
        this.pom?.dependencies?.dependencies?.forEach() {
            val name = it.artifactId
            val version = it.version
            val groupId : String = "$it.groupId"
            var dependency = Dependency( name, version )
            dependency.addProperty("groupId", groupId)
            dependencies.add(dependency)
        }
        return dependencies
    }
}