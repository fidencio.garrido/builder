package com.fidozen.builder.support

import com.fidozen.builder.configuration.Dependency

/**
 * Created by fido on 7/12/16.
 */
interface ConfigurationResource {
    fun addEntry(dependency: Dependency)
    fun addEntries(dependencies: List<Dependency>)
}