package com.fidozen.builder.support.helpers

import com.fidozen.builder.support.BuilderException
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException
import java.util.*

class TextFile(val file: File, val override: Boolean = true) {
    var content = ArrayList<String>()
    private var modificationsQueue: MutableMap<Int,String> = HashMap()

    init{
        this.readFile()
    }

    /**
     * Loads the content into an Array
     */
    fun readFile() = try{
        val inputFile = FileReader(file)
        val bufferedReader = BufferedReader(inputFile)
        val line: String
        for ( line in bufferedReader.lines() ) content.add(line)
    }catch(ex: IOException){
        throw BuilderException("Cannot read file to transform")
    }

    /**
     * Returns the list of lines that require to be modified in reverse order
     */
    protected fun getExecutionOrder(): List<Int>{
        val lines: MutableList<Int> = ArrayList()
        modificationsQueue.forEach {
            lines.add(it.key)
        }
        Collections.reverse(lines)
        return lines
    }

    /**
     * Writes the queue of changes
     */
    private fun writeFile(){
        val f = if (this.override) this.file else File(this.file.absolutePath + ".override")
        val builder = StringBuilder()
        f.createNewFile()
        this.content.forEach {
            builder.append(it)
            builder.append("\n")
        }
        f.writeText(builder.toString(),Charsets.UTF_8)
    }

    /**
     * Performs all the modifications in the file
     */
    fun modify(){
        getExecutionOrder().forEach {
            this.content.add(it, modificationsQueue.get(it)!!)
        }
        this.writeFile()
        resetModificationQueue()
    }

    /**
     * Retruns the line of a given string, first incidence
     * @param content A string to search for
     */
    fun getLineFor(content: String): Int{
        return this.content.indexOf(content)
    }

    /**
     * Returns the line of a partial string, first incidence
     * @param content A string to search for
     */
    fun getLineIfContains(content: String): Int{
        var ndx = -1;
        for (str: String in this.content){
            if (str.contains(content)){
                ndx = this.content.indexOf(str)
                break
            }
        }
        return ndx
    }

    /**
     * Inserts a line before the referred argument. In other words, it will push down the content of that line
     * @param line Line that will be used as reference, content will be added before it
     * @param text Text that will be added before the insertion point
     */
    fun insertBefore(line: Int, text: String){
        modificationsQueue.put(line, text)
    }

    /**
     * Inserts a line after the referred argument
     * @param line Line that will be used as reference, content will be added after it
     * @param text Text that will be added after the insertion point
     */
    fun insertAfter(line: Int, text: String){
        modificationsQueue.put(line +1, text)
    }

    /**
     * Cleaning mechanism that will be executed after the process is completed
     */
    private fun resetModificationQueue(){
        this.modificationsQueue = HashMap()
    }
}