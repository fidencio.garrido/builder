package com.fidozen.builder.configuration

import java.util.HashMap

data class Dependency(var name: String?, var version: String?){

    var meta: MutableMap<String, String>? = HashMap<String, String>()

    /**
     * Adds metadata without having to deal with the map
     * @param key Map key, i.e. groupId for Maven dependencies
     * @param value Map value, i.e. org.springframework for Maven dependencies
     */
    fun addProperty(key: String, value: String){
        this.meta?.put(key, value)
    }

    fun getPropertyValue(propertyName: String): String? = this.meta?.get(propertyName)

    /**
     * Compares name and metadata, the version is ignored
     */
    override fun equals(other: Any?): Boolean{
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Dependency

        if (name != other.name) return false
        if (meta != other.meta) return false

        return true
    }

    override fun hashCode(): Int{
        var result = name?.hashCode() ?: 0
        result = 31 * result + (version?.hashCode() ?: 0)
        result = 31 * result + (meta?.hashCode() ?: 0)
        return result
    }
}