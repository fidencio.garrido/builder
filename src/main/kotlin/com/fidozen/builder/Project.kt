package com.fidozen.builder

import java.io.File
import com.fidozen.builder.platforms.Platform
import com.fidozen.builder.platforms.Processor
import com.fidozen.builder.support.BuilderException

class Project(val platform: Platform){
    var projectPath: File?
    val processor: Processor

    init{
        this.processor = platform.getProcessor()
        projectPath = null
    }

    /**
     * Sets the root directory of the project to process
     */
    fun setInitialDir(path: String){
        val fileRef = File(path)
        if(fileRef.isDirectory()){
            projectPath = fileRef
            this.processor.setInitialDir(path)
        } else{
            throw BuilderException("The path does not exists")
        }
    }
}