package com.fidozen.builder.platforms.impl

import com.fidozen.builder.configuration.Dependency
import com.fidozen.builder.platforms.Processor

class NodeProcessor: Processor{

    override fun addDependency(dependency: Dependency) {
        throw UnsupportedOperationException()
    }

    override fun getDependencies(): List<Dependency> {
        throw UnsupportedOperationException()
    }

    override fun setInitialDir(path: String) {
        throw UnsupportedOperationException()
    }

    override fun excludeScan(globExpression: String) {
        throw UnsupportedOperationException()
    }

    override fun inject() {
        throw UnsupportedOperationException()
    }

}