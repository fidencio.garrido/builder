package com.fidozen.builder.platforms

import com.fidozen.builder.configuration.Dependency

interface Processor{
    fun addDependency(dependency: Dependency)

    fun getDependencies() : List<Dependency>

    fun setInitialDir(path: String)

    fun excludeScan(globExpression: String)

    fun inject(): Unit
}