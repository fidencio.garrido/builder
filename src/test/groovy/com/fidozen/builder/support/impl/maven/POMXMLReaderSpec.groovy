package com.fidozen.builder.support.impl.maven

import com.fidozen.builder.DefaultConfiguration
import com.fidozen.builder.configuration.Dependency
import spock.lang.Specification

class POMXMLReaderSpec extends Specification{
    def VALID_DIR = DefaultConfiguration.VALID_DIR

    def "Scenario: Reading valid POM file should find the right dependencies"(){
        given: "An existing pom"
            def marshaller = new POMXMLReader(VALID_DIR)
        when: "Trying to read the dependencies"
            List<Dependency> dependencies = marshaller.getDependencies()
        then: "List should retrieve existing dependencies"
            assert dependencies.size() > 0
    }
}
