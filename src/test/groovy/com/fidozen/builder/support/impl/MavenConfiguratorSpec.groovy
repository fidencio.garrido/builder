package com.fidozen.builder.support.impl

import com.fidozen.builder.configuration.Dependency
import spock.lang.Specification

class MavenConfiguratorSpec extends Specification{
    def "Scenario: Transforming a dependency object to XML"(){
        given: "A platform dependency"
            def dependency = new Dependency("spring-core","1.0")
            dependency.addProperty("groupId", "org.springframework")
        when: "converted to XML"
            def mavenConfigurator = new MavenConfigurator( "/" )
            def xml = mavenConfigurator.dependency2xml(dependency)
        then: "expect the resulting string to contains the right data"
            xml.contains( xmlfragment ) == expectedResult
        where: "data is defined as"
            xmlfragment                                 | expectedResult
            "<artifactId>spring-core</artifactId>"      | true
            "<groupId>org.springframework</groupId>"    | true
            "<version>1.0</version>"                    | true
            "<dependency>"                              | true
            "</dependency>"                             | true
            "<dependencies>"                            | false
    }
}
