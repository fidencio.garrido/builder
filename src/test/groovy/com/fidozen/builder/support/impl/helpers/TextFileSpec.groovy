package com.fidozen.builder.support.impl.helpers

import spock.lang.Specification
import java.io.File
import com.fidozen.builder.DefaultConfiguration
import com.fidozen.builder.support.helpers.TextFile

class TextFileSpec extends Specification {
    def curDir = DefaultConfiguration.CURRENT_DIR
    def testFolder = "${curDir}/src/test/resources"

    def "Scenario: Reading the content of a Java Class"(){
        given: "A valid Java class file"
            def file = new File(testFolder + "/simpleFiles/Example1.java")
            def textFile = new TextFile(file, false)
        when: "File is read"
        then: "Content should be correctly mapped"
            textFile.getLineIfContains( text ) == line
        where: "Text is defined as"
            text            |   line
            "public class"  | 2
            "import"        | 0
            "something"     | 3
            "nothere"       | -1
    }

    def "Scenario: Appending operations on a Java class"(){
        given: "A valid Java class file"
            def file = new File(testFolder + "/simpleFiles/Example1.java")
            def textFile = new TextFile(file, false)
        when: "Operations are queued 'randomly'"
            textFile.insertBefore(0, "//This is a comment")
            textFile.insertBefore(2, "@Annotation")
            textFile.insertAfter(4, "abc")
            textFile.insertAfter(2, "\t\tprivate int mynumber=1")
            def modificationList = textFile.getExecutionOrder()
        then: "Queue should be sorted accordingly"
            modificationList.get(ndx) == expected
        where: "Expected reverse order of lines to change"
            ndx |   expected
            0   |   5
            1   |   3
            2   |   2
            3   |   0
    }

    def "Scenario: Modifying content"(){
        given: "A valid class file"
            def file = new File(testFolder + "/simpleFiles/Example1.java")
            def textFile = new TextFile(file, false)
        when: "Operations are queued 'randomly'"
            textFile.insertBefore(0, "//This is a comment")
            textFile.insertBefore(2, "@Annotation")
            textFile.insertAfter(4, "abc")
            textFile.insertAfter(2, "\t\tprivate int mynumber=1")
            def modificationList = textFile.getExecutionOrder()
            textFile.modify()
        then: "Queue should be sorted accordingly"
            modificationList.get(ndx) == expected
        where: "Expected reverse order of lines to change"
            ndx |   expected
            0   |   5
            1   |   3
            2   |   2
            3   |   0
    }
}
