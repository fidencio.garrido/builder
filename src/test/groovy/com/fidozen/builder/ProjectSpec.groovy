package com.fidozen.builder

import spock.lang.Specification
import com.fidozen.builder.platforms.Platform
import com.fidozen.builder.support.BuilderException

class ProjectSpec extends Specification {
    def "Scenario: Right platform processor"(){
        given: "User selecting a specific platform"
            def javaProject = new Project(Platform.JAVA)
            def nodeProject = new Project(Platform.NODEJS)
        when: "Project target is read"
        then: "should return right platform"
            assert javaProject.getPlatform().toString() == "JAVA"

    }

    def "Scenario: Invalid directory"(){
        given: "A new project"
            def project = new Project(Platform.JAVA)
        when: "Project initial dir is invalid"
            def exceptionThrown = false
            try{
                project.setInitialDir("/none")
            }
            catch(BuilderException e){
                exceptionThrown = true
            }
        then: "BuilderException should be thrown"
            assert exceptionThrown == true
    }
}