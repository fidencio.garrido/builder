package com.fidozen.builder.configuration
/**
 * Created by fido on 7/12/16.
 */

import spock.lang.Specification

class DependencySpec extends Specification{
    def "Scenario: Equal dependencies"(){
        given: "Two dependencies"
            def name = "spring-core"
            def groupId = "org.springframework"
            def dep1 = new Dependency(name, "1.0")
            def dep2 = new Dependency(name, "2.0")
        when: "Name and metadata is the same"
            dep1.addProperty("groupId", groupId)
            dep2.addProperty("groupId", groupId)
        then: "Equals should be true"
            assert dep1.equals(dep2) == true
    }

    def "Scenario: Different metadata in dependency"(){
        given: "Two dependencies"
            def name = "spring-core"
            def version = "1.0"
            def dep1 = new Dependency(name, version)
            def dep2 = new Dependency(name, version)
        when: "Name and version are equal but metadata is not the same"
            dep1.addProperty("groupId", "a")
            dep2.addProperty("groupId", "b")
        then: "Equals should be false"
            assert dep1.equals(dep2) == false
    }
}
